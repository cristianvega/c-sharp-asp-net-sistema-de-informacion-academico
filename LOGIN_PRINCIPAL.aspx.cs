﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class LOGIN_PRINCIPAL : System.Web.UI.Page
{
    public ModelologinDataContext BaseDeDatos = new ModelologinDataContext();
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {

            string nombre = this.txtUsuario.Text;
            string clave = this.txtContraseña.Text;
            var datonit = from registronit in BaseDeDatos.Docente where registronit.Nombre_Docente == nombre && registronit.Contraseña == clave select registronit;
            if (datonit.Count() > 0)
            {
                
                Response.Redirect("Docente.master");
            }
            else
            {
                var datonit2 = from registronit in BaseDeDatos.Administrador where registronit.Nombre_Administrador == nombre && registronit.Contraseña == clave select registronit;
                if (datonit2.Count() > 0)
                {
                    
                    Response.Redirect("Administrador.master" );
                }
                else {
                    Response.Write("<script language='JavaScript'> alert('Usuario no existe, ingrese nombre y contraseña de nuevo');</script>");
                }


            }
        }
        catch (Exception ex)
        {
            Response.Write("<script language='JavaScript'> alert('Error en la informacion');</script>");
        }
    }
}