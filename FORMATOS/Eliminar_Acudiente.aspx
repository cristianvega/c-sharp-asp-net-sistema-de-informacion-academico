﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SITIOS MASTER/Administrador.master" AutoEventWireup="true" CodeFile="Eliminar_Acudiente.aspx.cs" Inherits="FORMATOS_Acudientes_Eliminar_Acudiente" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="Label1" runat="server" Text="Eliminar Acudiente"></asp:Label>
    </p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" DataKeyNames="Id_Acudiente" DataSourceID="SqlDataSource1">
            <Columns>
                <asp:CommandField ShowDeleteButton="True" />
                <asp:BoundField DataField="Id_Acudiente" HeaderText="Id_Acudiente" ReadOnly="True" SortExpression="Id_Acudiente" />
                <asp:BoundField DataField="Nombre_Acudiente" HeaderText="Nombre_Acudiente" SortExpression="Nombre_Acudiente" />
                <asp:BoundField DataField="Apellido_Acudiente" HeaderText="Apellido_Acudiente" SortExpression="Apellido_Acudiente" />
                <asp:BoundField DataField="Telefono" HeaderText="Telefono" SortExpression="Telefono" />
                <asp:BoundField DataField="Direccion" HeaderText="Direccion" SortExpression="Direccion" />
                <asp:BoundField DataField="fk_Estudiante" HeaderText="fk_Estudiante" SortExpression="fk_Estudiante" />
            </Columns>
            <FooterStyle BackColor="White" ForeColor="#000066" />
            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            <RowStyle ForeColor="#000066" />
            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#007DBB" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#00547E" />
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConflictDetection="CompareAllValues" ConnectionString="<%$ ConnectionStrings:BD_ProyectoWebConnectionString1 %>" DeleteCommand="DELETE FROM [Acudiente
] WHERE [Id_Acudiente] = @original_Id_Acudiente AND (([Nombre_Acudiente] = @original_Nombre_Acudiente) OR ([Nombre_Acudiente] IS NULL AND @original_Nombre_Acudiente IS NULL)) AND (([Apellido_Acudiente] = @original_Apellido_Acudiente) OR ([Apellido_Acudiente] IS NULL AND @original_Apellido_Acudiente IS NULL)) AND (([Telefono] = @original_Telefono) OR ([Telefono] IS NULL AND @original_Telefono IS NULL)) AND (([Direccion] = @original_Direccion) OR ([Direccion] IS NULL AND @original_Direccion IS NULL)) AND (([fk_Estudiante] = @original_fk_Estudiante) OR ([fk_Estudiante] IS NULL AND @original_fk_Estudiante IS NULL))" InsertCommand="INSERT INTO [Acudiente
] ([Id_Acudiente], [Nombre_Acudiente], [Apellido_Acudiente], [Telefono], [Direccion], [fk_Estudiante]) VALUES (@Id_Acudiente, @Nombre_Acudiente, @Apellido_Acudiente, @Telefono, @Direccion, @fk_Estudiante)" OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT * FROM [Acudiente
]" UpdateCommand="UPDATE [Acudiente
] SET [Nombre_Acudiente] = @Nombre_Acudiente, [Apellido_Acudiente] = @Apellido_Acudiente, [Telefono] = @Telefono, [Direccion] = @Direccion, [fk_Estudiante] = @fk_Estudiante WHERE [Id_Acudiente] = @original_Id_Acudiente AND (([Nombre_Acudiente] = @original_Nombre_Acudiente) OR ([Nombre_Acudiente] IS NULL AND @original_Nombre_Acudiente IS NULL)) AND (([Apellido_Acudiente] = @original_Apellido_Acudiente) OR ([Apellido_Acudiente] IS NULL AND @original_Apellido_Acudiente IS NULL)) AND (([Telefono] = @original_Telefono) OR ([Telefono] IS NULL AND @original_Telefono IS NULL)) AND (([Direccion] = @original_Direccion) OR ([Direccion] IS NULL AND @original_Direccion IS NULL)) AND (([fk_Estudiante] = @original_fk_Estudiante) OR ([fk_Estudiante] IS NULL AND @original_fk_Estudiante IS NULL))">
            <DeleteParameters>
                <asp:Parameter Name="original_Id_Acudiente" Type="Int64" />
                <asp:Parameter Name="original_Nombre_Acudiente" Type="String" />
                <asp:Parameter Name="original_Apellido_Acudiente" Type="String" />
                <asp:Parameter Name="original_Telefono" Type="Int64" />
                <asp:Parameter Name="original_Direccion" Type="String" />
                <asp:Parameter Name="original_fk_Estudiante" Type="Int64" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="Id_Acudiente" Type="Int64" />
                <asp:Parameter Name="Nombre_Acudiente" Type="String" />
                <asp:Parameter Name="Apellido_Acudiente" Type="String" />
                <asp:Parameter Name="Telefono" Type="Int64" />
                <asp:Parameter Name="Direccion" Type="String" />
                <asp:Parameter Name="fk_Estudiante" Type="Int64" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="Nombre_Acudiente" Type="String" />
                <asp:Parameter Name="Apellido_Acudiente" Type="String" />
                <asp:Parameter Name="Telefono" Type="Int64" />
                <asp:Parameter Name="Direccion" Type="String" />
                <asp:Parameter Name="fk_Estudiante" Type="Int64" />
                <asp:Parameter Name="original_Id_Acudiente" Type="Int64" />
                <asp:Parameter Name="original_Nombre_Acudiente" Type="String" />
                <asp:Parameter Name="original_Apellido_Acudiente" Type="String" />
                <asp:Parameter Name="original_Telefono" Type="Int64" />
                <asp:Parameter Name="original_Direccion" Type="String" />
                <asp:Parameter Name="original_fk_Estudiante" Type="Int64" />
            </UpdateParameters>
        </asp:SqlDataSource>
    </p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
</asp:Content>

