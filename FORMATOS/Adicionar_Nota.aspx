﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SITIOS MASTER/Administrador.master" AutoEventWireup="true" CodeFile="Adicionar_Nota.aspx.cs" Inherits="FORMATOS_Notas_Adicionar_Nota" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </p>
    <p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="Label1" runat="server" style="position: relative" Text="Adicionar Datos De La Nota"></asp:Label>
    </p>
    <p>
        &nbsp;</p>
    <p>
        <asp:DetailsView ID="DetailsView1" runat="server" AllowPaging="True" AutoGenerateRows="False" BackColor="White" BorderColor="#336666" BorderStyle="Double" BorderWidth="3px" CellPadding="4" DataKeyNames="Cod_Nota" DataSourceID="SqlDataSource1" GridLines="Horizontal" style="position: relative; top: 7px; left: 354px; height: 127px; width: 214px">
            <EditRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
            <Fields>
                <asp:BoundField DataField="Cod_Nota" HeaderText="Cod_Nota" ReadOnly="True" SortExpression="Cod_Nota" />
                <asp:BoundField DataField="fk_asignatura" HeaderText="fk_asignatura" SortExpression="fk_asignatura" />
                <asp:BoundField DataField="fk_estudiante" HeaderText="fk_estudiante" SortExpression="fk_estudiante" />
                <asp:BoundField DataField="Valor_Nota" HeaderText="Valor_Nota" SortExpression="Valor_Nota" />
            </Fields>
            <FooterStyle BackColor="White" ForeColor="#333333" />
            <HeaderStyle BackColor="#336666" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="White" ForeColor="#333333" />
        </asp:DetailsView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConflictDetection="CompareAllValues" ConnectionString="<%$ ConnectionStrings:BD_ProyectoWebConnectionString1 %>" DeleteCommand="DELETE FROM [Nota] WHERE [Cod_Nota] = @original_Cod_Nota AND (([fk_asignatura] = @original_fk_asignatura) OR ([fk_asignatura] IS NULL AND @original_fk_asignatura IS NULL)) AND (([fk_estudiante] = @original_fk_estudiante) OR ([fk_estudiante] IS NULL AND @original_fk_estudiante IS NULL)) AND (([Valor_Nota] = @original_Valor_Nota) OR ([Valor_Nota] IS NULL AND @original_Valor_Nota IS NULL))" InsertCommand="INSERT INTO [Nota] ([Cod_Nota], [fk_asignatura], [fk_estudiante], [Valor_Nota]) VALUES (@Cod_Nota, @fk_asignatura, @fk_estudiante, @Valor_Nota)" OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT * FROM [Nota]" UpdateCommand="UPDATE [Nota] SET [fk_asignatura] = @fk_asignatura, [fk_estudiante] = @fk_estudiante, [Valor_Nota] = @Valor_Nota WHERE [Cod_Nota] = @original_Cod_Nota AND (([fk_asignatura] = @original_fk_asignatura) OR ([fk_asignatura] IS NULL AND @original_fk_asignatura IS NULL)) AND (([fk_estudiante] = @original_fk_estudiante) OR ([fk_estudiante] IS NULL AND @original_fk_estudiante IS NULL)) AND (([Valor_Nota] = @original_Valor_Nota) OR ([Valor_Nota] IS NULL AND @original_Valor_Nota IS NULL))">
            <DeleteParameters>
                <asp:Parameter Name="original_Cod_Nota" Type="Int32" />
                <asp:Parameter Name="original_fk_asignatura" Type="Int32" />
                <asp:Parameter Name="original_fk_estudiante" Type="Int64" />
                <asp:Parameter Name="original_Valor_Nota" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="Cod_Nota" Type="Int32" />
                <asp:Parameter Name="fk_asignatura" Type="Int32" />
                <asp:Parameter Name="fk_estudiante" Type="Int64" />
                <asp:Parameter Name="Valor_Nota" Type="Int32" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="fk_asignatura" Type="Int32" />
                <asp:Parameter Name="fk_estudiante" Type="Int64" />
                <asp:Parameter Name="Valor_Nota" Type="Int32" />
                <asp:Parameter Name="original_Cod_Nota" Type="Int32" />
                <asp:Parameter Name="original_fk_asignatura" Type="Int32" />
                <asp:Parameter Name="original_fk_estudiante" Type="Int64" />
                <asp:Parameter Name="original_Valor_Nota" Type="Int32" />
            </UpdateParameters>
        </asp:SqlDataSource>
    </p>
    <p>
        &nbsp;</p>
</asp:Content>

