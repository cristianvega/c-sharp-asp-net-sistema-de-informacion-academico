﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SITIOS MASTER/Administrador.master" AutoEventWireup="true" CodeFile="Eliminar_Profesor.aspx.cs" Inherits="FORMATOS_Profesores_Eliminar_Profesor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="Label1" runat="server" style="position: relative" Text="Eliminar Docente"></asp:Label>
    </p>
    <p>
        &nbsp;</p>
    <p>
        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" DataKeyNames="Id_Docente" DataSourceID="SqlDataSource1" style="position: relative; top: -6px; left: 133px">
            <Columns>
                <asp:CommandField ShowDeleteButton="True" />
                <asp:BoundField DataField="Id_Docente" HeaderText="Id_Docente" ReadOnly="True" SortExpression="Id_Docente" />
                <asp:BoundField DataField="Nombre_Docente" HeaderText="Nombre_Docente" SortExpression="Nombre_Docente" />
                <asp:BoundField DataField="Apellido_Docente" HeaderText="Apellido_Docente" SortExpression="Apellido_Docente" />
                <asp:BoundField DataField="Correo" HeaderText="Correo" SortExpression="Correo" />
                <asp:BoundField DataField="Telefono_Docente" HeaderText="Telefono_Docente" SortExpression="Telefono_Docente" />
                <asp:BoundField DataField="Contraseña" HeaderText="Contraseña" SortExpression="Contraseña" />
            </Columns>
            <FooterStyle BackColor="White" ForeColor="#000066" />
            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            <RowStyle ForeColor="#000066" />
            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#007DBB" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#00547E" />
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConflictDetection="CompareAllValues" ConnectionString="<%$ ConnectionStrings:BD_ProyectoWebConnectionString1 %>" DeleteCommand="DELETE FROM [Docente] WHERE [Id_Docente] = @original_Id_Docente AND (([Nombre_Docente] = @original_Nombre_Docente) OR ([Nombre_Docente] IS NULL AND @original_Nombre_Docente IS NULL)) AND (([Apellido_Docente] = @original_Apellido_Docente) OR ([Apellido_Docente] IS NULL AND @original_Apellido_Docente IS NULL)) AND (([Correo] = @original_Correo) OR ([Correo] IS NULL AND @original_Correo IS NULL)) AND (([Telefono_Docente] = @original_Telefono_Docente) OR ([Telefono_Docente] IS NULL AND @original_Telefono_Docente IS NULL)) AND (([Contraseña] = @original_Contraseña) OR ([Contraseña] IS NULL AND @original_Contraseña IS NULL))" InsertCommand="INSERT INTO [Docente] ([Id_Docente], [Nombre_Docente], [Apellido_Docente], [Correo], [Telefono_Docente], [Contraseña]) VALUES (@Id_Docente, @Nombre_Docente, @Apellido_Docente, @Correo, @Telefono_Docente, @Contraseña)" OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT * FROM [Docente]" UpdateCommand="UPDATE [Docente] SET [Nombre_Docente] = @Nombre_Docente, [Apellido_Docente] = @Apellido_Docente, [Correo] = @Correo, [Telefono_Docente] = @Telefono_Docente, [Contraseña] = @Contraseña WHERE [Id_Docente] = @original_Id_Docente AND (([Nombre_Docente] = @original_Nombre_Docente) OR ([Nombre_Docente] IS NULL AND @original_Nombre_Docente IS NULL)) AND (([Apellido_Docente] = @original_Apellido_Docente) OR ([Apellido_Docente] IS NULL AND @original_Apellido_Docente IS NULL)) AND (([Correo] = @original_Correo) OR ([Correo] IS NULL AND @original_Correo IS NULL)) AND (([Telefono_Docente] = @original_Telefono_Docente) OR ([Telefono_Docente] IS NULL AND @original_Telefono_Docente IS NULL)) AND (([Contraseña] = @original_Contraseña) OR ([Contraseña] IS NULL AND @original_Contraseña IS NULL))">
            <DeleteParameters>
                <asp:Parameter Name="original_Id_Docente" Type="Int64" />
                <asp:Parameter Name="original_Nombre_Docente" Type="String" />
                <asp:Parameter Name="original_Apellido_Docente" Type="String" />
                <asp:Parameter Name="original_Correo" Type="String" />
                <asp:Parameter Name="original_Telefono_Docente" Type="Int64" />
                <asp:Parameter Name="original_Contraseña" Type="String" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="Id_Docente" Type="Int64" />
                <asp:Parameter Name="Nombre_Docente" Type="String" />
                <asp:Parameter Name="Apellido_Docente" Type="String" />
                <asp:Parameter Name="Correo" Type="String" />
                <asp:Parameter Name="Telefono_Docente" Type="Int64" />
                <asp:Parameter Name="Contraseña" Type="String" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="Nombre_Docente" Type="String" />
                <asp:Parameter Name="Apellido_Docente" Type="String" />
                <asp:Parameter Name="Correo" Type="String" />
                <asp:Parameter Name="Telefono_Docente" Type="Int64" />
                <asp:Parameter Name="Contraseña" Type="String" />
                <asp:Parameter Name="original_Id_Docente" Type="Int64" />
                <asp:Parameter Name="original_Nombre_Docente" Type="String" />
                <asp:Parameter Name="original_Apellido_Docente" Type="String" />
                <asp:Parameter Name="original_Correo" Type="String" />
                <asp:Parameter Name="original_Telefono_Docente" Type="Int64" />
                <asp:Parameter Name="original_Contraseña" Type="String" />
            </UpdateParameters>
        </asp:SqlDataSource>
    </p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
</asp:Content>

