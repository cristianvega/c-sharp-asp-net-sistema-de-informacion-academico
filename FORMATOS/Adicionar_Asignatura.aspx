﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SITIOS MASTER/Administrador.master" AutoEventWireup="true" CodeFile="Adicionar_Asignatura.aspx.cs" Inherits="FORMATOS_Asignaturas_Adicionar_Asignatura" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    adicionar asignatura&nbsp;</p>
    <asp:DetailsView ID="DetailsView1" runat="server" AllowPaging="True" AutoGenerateRows="False" BackColor="White" BorderColor="#336666" BorderStyle="Double" BorderWidth="3px" CellPadding="4" DataKeyNames="Id_Acudiente" DataSourceID="SqlDataSource1" GridLines="Horizontal" Height="50px" style="position: relative; top: 23px; left: 464px; width: 251px">
        <EditRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
        <Fields>
            <asp:BoundField DataField="Id_Acudiente" HeaderText="Id_Acudiente" ReadOnly="True" SortExpression="Id_Acudiente" />
            <asp:BoundField DataField="Nombre_Acudiente" HeaderText="Nombre_Acudiente" SortExpression="Nombre_Acudiente" />
            <asp:BoundField DataField="Apellido_Acudiente" HeaderText="Apellido_Acudiente" SortExpression="Apellido_Acudiente" />
            <asp:BoundField DataField="Telefono" HeaderText="Telefono" SortExpression="Telefono" />
            <asp:BoundField DataField="Direccion" HeaderText="Direccion" SortExpression="Direccion" />
            <asp:BoundField DataField="fk_Estudiante" HeaderText="fk_Estudiante" SortExpression="fk_Estudiante" />
            <asp:CommandField ShowInsertButton="True" />
        </Fields>
        <FooterStyle BackColor="White" ForeColor="#333333" />
        <HeaderStyle BackColor="#336666" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="White" ForeColor="#333333" />
    </asp:DetailsView>
    <p>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConflictDetection="CompareAllValues" ConnectionString="<%$ ConnectionStrings:BD_ProyectoWebConnectionString1 %>" DeleteCommand="DELETE FROM [Acudiente
] WHERE [Id_Acudiente] = @original_Id_Acudiente AND (([Nombre_Acudiente] = @original_Nombre_Acudiente) OR ([Nombre_Acudiente] IS NULL AND @original_Nombre_Acudiente IS NULL)) AND (([Apellido_Acudiente] = @original_Apellido_Acudiente) OR ([Apellido_Acudiente] IS NULL AND @original_Apellido_Acudiente IS NULL)) AND (([Telefono] = @original_Telefono) OR ([Telefono] IS NULL AND @original_Telefono IS NULL)) AND (([Direccion] = @original_Direccion) OR ([Direccion] IS NULL AND @original_Direccion IS NULL)) AND (([fk_Estudiante] = @original_fk_Estudiante) OR ([fk_Estudiante] IS NULL AND @original_fk_Estudiante IS NULL))" InsertCommand="INSERT INTO [Acudiente
] ([Id_Acudiente], [Nombre_Acudiente], [Apellido_Acudiente], [Telefono], [Direccion], [fk_Estudiante]) VALUES (@Id_Acudiente, @Nombre_Acudiente, @Apellido_Acudiente, @Telefono, @Direccion, @fk_Estudiante)" OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT * FROM [Acudiente
]" UpdateCommand="UPDATE [Acudiente
] SET [Nombre_Acudiente] = @Nombre_Acudiente, [Apellido_Acudiente] = @Apellido_Acudiente, [Telefono] = @Telefono, [Direccion] = @Direccion, [fk_Estudiante] = @fk_Estudiante WHERE [Id_Acudiente] = @original_Id_Acudiente AND (([Nombre_Acudiente] = @original_Nombre_Acudiente) OR ([Nombre_Acudiente] IS NULL AND @original_Nombre_Acudiente IS NULL)) AND (([Apellido_Acudiente] = @original_Apellido_Acudiente) OR ([Apellido_Acudiente] IS NULL AND @original_Apellido_Acudiente IS NULL)) AND (([Telefono] = @original_Telefono) OR ([Telefono] IS NULL AND @original_Telefono IS NULL)) AND (([Direccion] = @original_Direccion) OR ([Direccion] IS NULL AND @original_Direccion IS NULL)) AND (([fk_Estudiante] = @original_fk_Estudiante) OR ([fk_Estudiante] IS NULL AND @original_fk_Estudiante IS NULL))">
            <DeleteParameters>
                <asp:Parameter Name="original_Id_Acudiente" Type="Int64" />
                <asp:Parameter Name="original_Nombre_Acudiente" Type="String" />
                <asp:Parameter Name="original_Apellido_Acudiente" Type="String" />
                <asp:Parameter Name="original_Telefono" Type="Int64" />
                <asp:Parameter Name="original_Direccion" Type="String" />
                <asp:Parameter Name="original_fk_Estudiante" Type="Int64" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="Id_Acudiente" Type="Int64" />
                <asp:Parameter Name="Nombre_Acudiente" Type="String" />
                <asp:Parameter Name="Apellido_Acudiente" Type="String" />
                <asp:Parameter Name="Telefono" Type="Int64" />
                <asp:Parameter Name="Direccion" Type="String" />
                <asp:Parameter Name="fk_Estudiante" Type="Int64" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="Nombre_Acudiente" Type="String" />
                <asp:Parameter Name="Apellido_Acudiente" Type="String" />
                <asp:Parameter Name="Telefono" Type="Int64" />
                <asp:Parameter Name="Direccion" Type="String" />
                <asp:Parameter Name="fk_Estudiante" Type="Int64" />
                <asp:Parameter Name="original_Id_Acudiente" Type="Int64" />
                <asp:Parameter Name="original_Nombre_Acudiente" Type="String" />
                <asp:Parameter Name="original_Apellido_Acudiente" Type="String" />
                <asp:Parameter Name="original_Telefono" Type="Int64" />
                <asp:Parameter Name="original_Direccion" Type="String" />
                <asp:Parameter Name="original_fk_Estudiante" Type="Int64" />
            </UpdateParameters>
        </asp:SqlDataSource>
    </p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
</asp:Content>

