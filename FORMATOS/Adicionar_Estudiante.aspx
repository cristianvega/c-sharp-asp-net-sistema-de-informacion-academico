﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SITIOS MASTER/Administrador.master" AutoEventWireup="true" CodeFile="Adicionar_Estudiante.aspx.cs" Inherits="FORMATOS_Estudiantes_Adicionar_Estudiante" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <p>
        &nbsp;</p>
    <p>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:Label ID="Label7" runat="server" Text="Bienvenido al registro de estudiantes"></asp:Label>
    </p>
    <p>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:DetailsView ID="DetailsView1" runat="server" AllowPaging="True" AutoGenerateRows="False" BackColor="White" BorderColor="#336666" BorderStyle="Double" BorderWidth="3px" CellPadding="4" DataKeyNames="Id_Estudiante" DataSourceID="SqlDataSource1" GridLines="Horizontal" Height="50px" style="position: relative; top: 4px; left: 425px; width: 260px">
            <EditRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
            <Fields>
                <asp:BoundField DataField="Id_Estudiante" HeaderText="Id_Estudiante" ReadOnly="True" SortExpression="Id_Estudiante" />
                <asp:BoundField DataField="Nombre_Estudiante" HeaderText="Nombre_Estudiante" SortExpression="Nombre_Estudiante" />
                <asp:BoundField DataField="Apellido_Estudiante" HeaderText="Apellido_Estudiante" SortExpression="Apellido_Estudiante" />
                <asp:BoundField DataField="Fecha_Nacimiento" HeaderText="Fecha_Nacimiento" SortExpression="Fecha_Nacimiento" />
                <asp:BoundField DataField="Correo" HeaderText="Correo" SortExpression="Correo" />
                <asp:BoundField DataField="Direccion" HeaderText="Direccion" SortExpression="Direccion" />
                <asp:CommandField ShowInsertButton="True" />
            </Fields>
            <FooterStyle BackColor="White" ForeColor="#333333" />
            <HeaderStyle BackColor="#336666" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="White" ForeColor="#333333" />
        </asp:DetailsView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConflictDetection="CompareAllValues" ConnectionString="<%$ ConnectionStrings:BD_ProyectoWebConnectionString1 %>" DeleteCommand="DELETE FROM [Estudiante] WHERE [Id_Estudiante] = @original_Id_Estudiante AND (([Nombre_Estudiante] = @original_Nombre_Estudiante) OR ([Nombre_Estudiante] IS NULL AND @original_Nombre_Estudiante IS NULL)) AND (([Apellido_Estudiante] = @original_Apellido_Estudiante) OR ([Apellido_Estudiante] IS NULL AND @original_Apellido_Estudiante IS NULL)) AND (([Fecha_Nacimiento] = @original_Fecha_Nacimiento) OR ([Fecha_Nacimiento] IS NULL AND @original_Fecha_Nacimiento IS NULL)) AND (([Correo] = @original_Correo) OR ([Correo] IS NULL AND @original_Correo IS NULL)) AND (([Direccion] = @original_Direccion) OR ([Direccion] IS NULL AND @original_Direccion IS NULL))" InsertCommand="INSERT INTO [Estudiante] ([Id_Estudiante], [Nombre_Estudiante], [Apellido_Estudiante], [Fecha_Nacimiento], [Correo], [Direccion]) VALUES (@Id_Estudiante, @Nombre_Estudiante, @Apellido_Estudiante, @Fecha_Nacimiento, @Correo, @Direccion)" OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT * FROM [Estudiante]" UpdateCommand="UPDATE [Estudiante] SET [Nombre_Estudiante] = @Nombre_Estudiante, [Apellido_Estudiante] = @Apellido_Estudiante, [Fecha_Nacimiento] = @Fecha_Nacimiento, [Correo] = @Correo, [Direccion] = @Direccion WHERE [Id_Estudiante] = @original_Id_Estudiante AND (([Nombre_Estudiante] = @original_Nombre_Estudiante) OR ([Nombre_Estudiante] IS NULL AND @original_Nombre_Estudiante IS NULL)) AND (([Apellido_Estudiante] = @original_Apellido_Estudiante) OR ([Apellido_Estudiante] IS NULL AND @original_Apellido_Estudiante IS NULL)) AND (([Fecha_Nacimiento] = @original_Fecha_Nacimiento) OR ([Fecha_Nacimiento] IS NULL AND @original_Fecha_Nacimiento IS NULL)) AND (([Correo] = @original_Correo) OR ([Correo] IS NULL AND @original_Correo IS NULL)) AND (([Direccion] = @original_Direccion) OR ([Direccion] IS NULL AND @original_Direccion IS NULL))">
            <DeleteParameters>
                <asp:Parameter Name="original_Id_Estudiante" Type="Int64" />
                <asp:Parameter Name="original_Nombre_Estudiante" Type="String" />
                <asp:Parameter Name="original_Apellido_Estudiante" Type="String" />
                <asp:Parameter DbType="Date" Name="original_Fecha_Nacimiento" />
                <asp:Parameter Name="original_Correo" Type="String" />
                <asp:Parameter Name="original_Direccion" Type="String" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="Id_Estudiante" Type="Int64" />
                <asp:Parameter Name="Nombre_Estudiante" Type="String" />
                <asp:Parameter Name="Apellido_Estudiante" Type="String" />
                <asp:Parameter DbType="Date" Name="Fecha_Nacimiento" />
                <asp:Parameter Name="Correo" Type="String" />
                <asp:Parameter Name="Direccion" Type="String" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="Nombre_Estudiante" Type="String" />
                <asp:Parameter Name="Apellido_Estudiante" Type="String" />
                <asp:Parameter DbType="Date" Name="Fecha_Nacimiento" />
                <asp:Parameter Name="Correo" Type="String" />
                <asp:Parameter Name="Direccion" Type="String" />
                <asp:Parameter Name="original_Id_Estudiante" Type="Int64" />
                <asp:Parameter Name="original_Nombre_Estudiante" Type="String" />
                <asp:Parameter Name="original_Apellido_Estudiante" Type="String" />
                <asp:Parameter DbType="Date" Name="original_Fecha_Nacimiento" />
                <asp:Parameter Name="original_Correo" Type="String" />
                <asp:Parameter Name="original_Direccion" Type="String" />
            </UpdateParameters>
        </asp:SqlDataSource>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    </p>
    <p>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;</p>
    <p>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
</asp:Content>

