﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SITIOS MASTER/Administrador.master" AutoEventWireup="true" CodeFile="Eliminar_Asignatura.aspx.cs" Inherits="FORMATOS_Asignaturas_Eliminar_Asignatura" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="Label1" runat="server" Text="Eliminar Datos  De La Asignatura"></asp:Label>
    </p>
    <p>
        &nbsp;</p>
    <p>
        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" DataKeyNames="Cod_Asignatura" DataSourceID="SqlDataSource1" style="position: relative; top: -2px; left: 159px">
            <Columns>
                <asp:CommandField ShowDeleteButton="True" />
                <asp:BoundField DataField="Cod_Asignatura" HeaderText="Cod_Asignatura" ReadOnly="True" SortExpression="Cod_Asignatura" />
                <asp:BoundField DataField="Nombre_Asignatura" HeaderText="Nombre_Asignatura" SortExpression="Nombre_Asignatura" />
                <asp:BoundField DataField="Intensidad_Horas" HeaderText="Intensidad_Horas" SortExpression="Intensidad_Horas" />
                <asp:BoundField DataField="Capacidad_Estudiantes" HeaderText="Capacidad_Estudiantes" SortExpression="Capacidad_Estudiantes" />
                <asp:BoundField DataField="fk_docente" HeaderText="fk_docente" SortExpression="fk_docente" />
            </Columns>
            <FooterStyle BackColor="White" ForeColor="#000066" />
            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            <RowStyle ForeColor="#000066" />
            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#007DBB" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#00547E" />
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConflictDetection="CompareAllValues" ConnectionString="<%$ ConnectionStrings:BD_ProyectoWebConnectionString1 %>" DeleteCommand="DELETE FROM [Asignatura] WHERE [Cod_Asignatura] = @original_Cod_Asignatura AND (([Nombre_Asignatura] = @original_Nombre_Asignatura) OR ([Nombre_Asignatura] IS NULL AND @original_Nombre_Asignatura IS NULL)) AND (([Intensidad_Horas] = @original_Intensidad_Horas) OR ([Intensidad_Horas] IS NULL AND @original_Intensidad_Horas IS NULL)) AND (([Capacidad_Estudiantes] = @original_Capacidad_Estudiantes) OR ([Capacidad_Estudiantes] IS NULL AND @original_Capacidad_Estudiantes IS NULL)) AND (([fk_docente] = @original_fk_docente) OR ([fk_docente] IS NULL AND @original_fk_docente IS NULL))" InsertCommand="INSERT INTO [Asignatura] ([Cod_Asignatura], [Nombre_Asignatura], [Intensidad_Horas], [Capacidad_Estudiantes], [fk_docente]) VALUES (@Cod_Asignatura, @Nombre_Asignatura, @Intensidad_Horas, @Capacidad_Estudiantes, @fk_docente)" OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT * FROM [Asignatura]" UpdateCommand="UPDATE [Asignatura] SET [Nombre_Asignatura] = @Nombre_Asignatura, [Intensidad_Horas] = @Intensidad_Horas, [Capacidad_Estudiantes] = @Capacidad_Estudiantes, [fk_docente] = @fk_docente WHERE [Cod_Asignatura] = @original_Cod_Asignatura AND (([Nombre_Asignatura] = @original_Nombre_Asignatura) OR ([Nombre_Asignatura] IS NULL AND @original_Nombre_Asignatura IS NULL)) AND (([Intensidad_Horas] = @original_Intensidad_Horas) OR ([Intensidad_Horas] IS NULL AND @original_Intensidad_Horas IS NULL)) AND (([Capacidad_Estudiantes] = @original_Capacidad_Estudiantes) OR ([Capacidad_Estudiantes] IS NULL AND @original_Capacidad_Estudiantes IS NULL)) AND (([fk_docente] = @original_fk_docente) OR ([fk_docente] IS NULL AND @original_fk_docente IS NULL))">
            <DeleteParameters>
                <asp:Parameter Name="original_Cod_Asignatura" Type="Int32" />
                <asp:Parameter Name="original_Nombre_Asignatura" Type="String" />
                <asp:Parameter Name="original_Intensidad_Horas" Type="Int32" />
                <asp:Parameter Name="original_Capacidad_Estudiantes" Type="Int32" />
                <asp:Parameter Name="original_fk_docente" Type="Int64" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="Cod_Asignatura" Type="Int32" />
                <asp:Parameter Name="Nombre_Asignatura" Type="String" />
                <asp:Parameter Name="Intensidad_Horas" Type="Int32" />
                <asp:Parameter Name="Capacidad_Estudiantes" Type="Int32" />
                <asp:Parameter Name="fk_docente" Type="Int64" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="Nombre_Asignatura" Type="String" />
                <asp:Parameter Name="Intensidad_Horas" Type="Int32" />
                <asp:Parameter Name="Capacidad_Estudiantes" Type="Int32" />
                <asp:Parameter Name="fk_docente" Type="Int64" />
                <asp:Parameter Name="original_Cod_Asignatura" Type="Int32" />
                <asp:Parameter Name="original_Nombre_Asignatura" Type="String" />
                <asp:Parameter Name="original_Intensidad_Horas" Type="Int32" />
                <asp:Parameter Name="original_Capacidad_Estudiantes" Type="Int32" />
                <asp:Parameter Name="original_fk_docente" Type="Int64" />
            </UpdateParameters>
        </asp:SqlDataSource>
    </p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
</asp:Content>

