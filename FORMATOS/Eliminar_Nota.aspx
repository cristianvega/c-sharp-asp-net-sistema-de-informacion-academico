﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SITIOS MASTER/Administrador.master" AutoEventWireup="true" CodeFile="Eliminar_Nota.aspx.cs" Inherits="FORMATOS_Notas_Eliminar_Nota" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="Label1" runat="server" Text="Eliminar Datos De La Nota"></asp:Label>
    </p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" DataKeyNames="Cod_Nota" DataSourceID="SqlDataSource1" style="position: relative; top: -13px; left: 376px">
            <Columns>
                <asp:CommandField ShowDeleteButton="True" />
                <asp:BoundField DataField="Cod_Nota" HeaderText="Cod_Nota" ReadOnly="True" SortExpression="Cod_Nota" />
                <asp:BoundField DataField="fk_asignatura" HeaderText="fk_asignatura" SortExpression="fk_asignatura" />
                <asp:BoundField DataField="fk_estudiante" HeaderText="fk_estudiante" SortExpression="fk_estudiante" />
                <asp:BoundField DataField="Valor_Nota" HeaderText="Valor_Nota" SortExpression="Valor_Nota" />
            </Columns>
            <FooterStyle BackColor="White" ForeColor="#000066" />
            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            <RowStyle ForeColor="#000066" />
            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#007DBB" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#00547E" />
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConflictDetection="CompareAllValues" ConnectionString="<%$ ConnectionStrings:BD_ProyectoWebConnectionString1 %>" DeleteCommand="DELETE FROM [Nota] WHERE [Cod_Nota] = @original_Cod_Nota AND (([fk_asignatura] = @original_fk_asignatura) OR ([fk_asignatura] IS NULL AND @original_fk_asignatura IS NULL)) AND (([fk_estudiante] = @original_fk_estudiante) OR ([fk_estudiante] IS NULL AND @original_fk_estudiante IS NULL)) AND (([Valor_Nota] = @original_Valor_Nota) OR ([Valor_Nota] IS NULL AND @original_Valor_Nota IS NULL))" InsertCommand="INSERT INTO [Nota] ([Cod_Nota], [fk_asignatura], [fk_estudiante], [Valor_Nota]) VALUES (@Cod_Nota, @fk_asignatura, @fk_estudiante, @Valor_Nota)" OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT * FROM [Nota]" UpdateCommand="UPDATE [Nota] SET [fk_asignatura] = @fk_asignatura, [fk_estudiante] = @fk_estudiante, [Valor_Nota] = @Valor_Nota WHERE [Cod_Nota] = @original_Cod_Nota AND (([fk_asignatura] = @original_fk_asignatura) OR ([fk_asignatura] IS NULL AND @original_fk_asignatura IS NULL)) AND (([fk_estudiante] = @original_fk_estudiante) OR ([fk_estudiante] IS NULL AND @original_fk_estudiante IS NULL)) AND (([Valor_Nota] = @original_Valor_Nota) OR ([Valor_Nota] IS NULL AND @original_Valor_Nota IS NULL))">
            <DeleteParameters>
                <asp:Parameter Name="original_Cod_Nota" Type="Int32" />
                <asp:Parameter Name="original_fk_asignatura" Type="Int32" />
                <asp:Parameter Name="original_fk_estudiante" Type="Int64" />
                <asp:Parameter Name="original_Valor_Nota" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="Cod_Nota" Type="Int32" />
                <asp:Parameter Name="fk_asignatura" Type="Int32" />
                <asp:Parameter Name="fk_estudiante" Type="Int64" />
                <asp:Parameter Name="Valor_Nota" Type="Int32" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="fk_asignatura" Type="Int32" />
                <asp:Parameter Name="fk_estudiante" Type="Int64" />
                <asp:Parameter Name="Valor_Nota" Type="Int32" />
                <asp:Parameter Name="original_Cod_Nota" Type="Int32" />
                <asp:Parameter Name="original_fk_asignatura" Type="Int32" />
                <asp:Parameter Name="original_fk_estudiante" Type="Int64" />
                <asp:Parameter Name="original_Valor_Nota" Type="Int32" />
            </UpdateParameters>
        </asp:SqlDataSource>
    </p>
</asp:Content>

